# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam projects=[ ]
myexparam check_target="check-all"
myexparam -b slotted=false
myexparam -b asserts=false
myexparam -b rtlib=false
myexparam -b stdlib=false

exparam -v PROJECTS projects[@]
exparam -v CHECK_TARGET check_target

MY_PN="${PROJECTS[0]}"
if ever at_least "9.0.1_rc1"; then
    MY_PV="${PV/_}"
else
    MY_PV="${PV}"
fi
MY_PNV="${MY_PN}-${MY_PV}"

if [[ "${PN}" != "llvm" ]]; then
    PROJECTS+=( "llvm" )
fi

DOWNLOADS=""

if ever is_scm ; then
    SCM_CHECKOUT_TO="llvm-project"
    SCM_UNPACK_TO="llvm-project"
    require github [ user="llvm" pn="llvm-project" ]
elif ever at_least "9.0.1_rc1"; then
    DOWNLOADS="https://github.com/llvm/llvm-project/releases/download/llvmorg-${PV/_/-}/llvm-project-${MY_PV}.tar.xz"
else
    for proj in "${PROJECTS[@]}"; do
        [[ "${proj}" == "clang" ]] && proj="cfe"
        if [[ ${PV} == 8.0.1 ]]; then
            DOWNLOADS+=" https://github.com/llvm/llvm-project/releases/download/llvmorg-${PV/_/-}/${proj}-${MY_PV}.src.tar.xz"
        elif [[ ${PV} == *rc* ]]; then
            DOWNLOADS+=" https://prereleases.llvm.org/${PV%*rc*}/rc${PV#*rc}/${proj}-${PV}.src.tar.xz"
        else
            DOWNLOADS+=" https://llvm.org/releases/${PV}/${proj}-${PV}.src.tar.xz"
        fi
    done
fi

require cmake [ ninja=true ]
if [[ "${PN}" == "llvm" ]]; then
    require setup-py [ import=distutils blacklist=none has_bin=true multibuild=false ]
else
    require python [ blacklist=none multibuild=false ]
fi
require utf8-locale

CMAKE_SOURCE="${WORKBASE}/llvm-project/${MY_PN}"

HOMEPAGE="https://${MY_PN}.llvm.org/"

if ever at_least 9.0; then
    LICENCES="Apache-2.0-with-LLVM-exception"
else
    LICENCES="|| ( MIT UoI-NCSA )"
fi

# See http://blog.llvm.org/2016/12/llvms-new-versioning-scheme.html for more info
# In "X.Y.Z", X is major release, Y is minor release, and Z is "patch" release
# Major version is the slot, except for any LLVM with major-release < 4
if [[ ${PV} == scm ]]; then
    LLVM_SLOT="10"
elif ever is_scm ; then
    LLVM_SLOT="${PV%-scm}"
else
    LLVM_SLOT="$(ever major)"
fi

if exparam -b slotted; then
    SLOT="${LLVM_SLOT}"
else
    SLOT="0"
fi

if exparam -b asserts; then
    MYOPTIONS="asserts [[ description = [ Enable assertions ] ]]"
else
    MYOPTIONS=""
fi
DEPENDENCIES=""

if exparam -b rtlib; then
    MYOPTIONS+="
        ( providers: compiler-rt libgcc ) [[ number-selected = exactly-one ]]
    "
    DEPENDENCIES+="
        providers:compiler-rt? (
            dev-libs/compiler-rt:=
            sys-libs/llvm-libunwind
        )
        providers:libgcc? ( sys-libs/libgcc:= )
    "
fi

if exparam -b stdlib; then
    MYOPTIONS+="
        ( providers: libc++ libstdc++ ) [[ number-selected = exactly-one ]]
    "
    DEPENDENCIES+="
        providers:libc++? (
            sys-libs/libc++
            sys-libs/libc++abi
        )
        providers:libstdc++? ( sys-libs/libstdc++:= )
    "
fi

if [[ "${MY_PN}" != "llvm" ]]; then
    DEPENDENCIES+="
        build+test:
            dev-lang/llvm:${LLVM_SLOT} [[ note = [ For llvm-config and CMake modules ] ]]
    "
fi

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${LLVM_SLOT}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DPYTHON_EXECUTABLE:PATH="${PYTHON}"

    -DLLVM_MAIN_SRC_DIR="${WORKBASE}/llvm-project/llvm"
    -DLLVM_LIT_ARGS:STRING="-sv"

    # This only controls whether or not LLVM's utils link against the dylib.
    -DLLVM_LINK_LLVM_DYLIB:BOOL=ON

    # Enable RTTI by default, Upstream and projects which need it (like mesa)
    # strongly recommend enabling it. Enabling it only costs a little disk space.
    -DLLVM_ENABLE_RTTI:BOOL=ON

    # Enable exception handling by default, it is basically free to build this
    # and some projects depend on it.
    -DLLVM_ENABLE_EH:BOOL=ON

    -DLLVM_ENABLE_PIC:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS=()

if exparam -b asserts; then
    CMAKE_SRC_CONFIGURE_OPTIONS+=(
        'asserts LLVM_ENABLE_ASSERTIONS'
    )
fi

export_exlib_phases pkg_setup src_unpack src_test

llvm-project_pkg_setup() {
    default

    # Require UTF-8 locale for python tests dealing with Unicode
    require_utf8_locale
}

llvm-project_src_unpack() {
    cmake_src_unpack
    if ever at_least "9.0.1_rc1"; then
        edo ln -s "${WORKBASE}"/llvm-project{-${MY_PV},}
    elif ! ever is_scm; then
        edo mkdir -p "${WORKBASE}"/llvm-project
        for proj in "${PROJECTS[@]}"; do
            src="${proj}"
            ! ever at_least "9.0.1_rc1" && [[ "${proj}" == "clang" ]] && src="cfe"
            edo ln -s "${WORKBASE}"/${src}-${MY_PV}.src "${WORKBASE}"/llvm-project/${proj}
        done
    fi
}

llvm-project_src_test() {
    require_utf8_locale
    eninja "${CHECK_TARGET}"
}
