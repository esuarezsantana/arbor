# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ projects=[ "${PN}" ] ]
require alternatives providers

SUMMARY="The LLVM linker"

# lld is a cross linker and can compile for any target without a
# new binary being compiled for the target. this is only used for the
# creation of the symlinks for targets; /usr/${CHOST}/bin/${target}-lld
CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS+="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

providers=( "llvm $(ever major)" )

export_exlib_phases src_configure src_install src_test

lld_src_configure() {
    providers_set
    cmake_src_configure
}

lld_src_install() {
    local host=$(exhost --target)
    local target

    cmake_src_install

    # ban ld.lld, alternative for banned ld
    dobanned ld.lld

    # eclectic managed files
    local em=(
        /usr/${host}/bin/ld
        "${BANNEDDIR}"/ld
    )

    # provide host-prefixed ld.lld
    for target in ${CROSS_COMPILE_TARGETS}; do
        if option targets:${target}; then
            dosym lld /usr/${host}/bin/${target}-ld.lld
            em+=( /usr/${host}/bin/${target}-ld )
        fi
    done

    # alternatives setup
    local alternatives=()
    for m in "${em[@]}"; do
        alternatives+=( "${m}" "${m##*/}".lld )
    done

    alternatives_for ld lld 10 "${alternatives[@]}"
}

lld_src_test() {
    # There doesn't seem to be a test target
    :
}
