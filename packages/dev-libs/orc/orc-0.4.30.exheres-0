# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="The Oil Runtime Compiler"
DESCRIPTION="
Orc is a library and set of tools for compiling and executing very simple programs that operate on
arrays of data.  The “language” is a generic assembly language that represents many of the features
available in SIMD architectures, including saturated addition and subtraction, and many arithmetic
operations.
"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="BSD-2 BSD-3"
SLOT="0.4"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gtk-doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbenchmarks=enabled
    -Dexamples=enabled
    -Dorc-backend=all
    -Dorc-test=enabled
    -Dtools=enabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gtk-doc gtk_doc'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

