Upstream: https://reviews.llvm.org/D70232
From cd26ec143d0fd7a45983b5794c4fc7750162c7a9 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Thu, 14 Nov 2019 12:16:04 +0100
Subject: [PATCH] compiler-rt: use correct builtins library when linking
 sanitizers

When building on x86_64 with -DCOMPILER_RT_USE_BUILTINS_LIBRARY=ON
we try to build sanitizers for both x86_64 and i386 but we tried to
link both to the x86_64 builtins.
This patch uses the correct builtins to link into the sanitizers.

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 compiler-rt/CMakeLists.txt                       |  1 -
 compiler-rt/cmake/Modules/AddCompilerRT.cmake    | 11 +++++++++++
 compiler-rt/cmake/Modules/HandleCompilerRT.cmake |  6 ++++--
 compiler-rt/cmake/config-ix.cmake                |  2 +-
 4 files changed, 16 insertions(+), 4 deletions(-)

diff --git a/compiler-rt/CMakeLists.txt b/compiler-rt/CMakeLists.txt
index 8d768a404f2..4a572cefbdd 100644
--- a/compiler-rt/CMakeLists.txt
+++ b/compiler-rt/CMakeLists.txt
@@ -397,7 +397,6 @@ append_list_if(COMPILER_RT_HAS_NODEFAULTLIBS_FLAG -nodefaultlibs SANITIZER_COMMO
 append_list_if(COMPILER_RT_HAS_Z_TEXT -Wl,-z,text SANITIZER_COMMON_LINK_FLAGS)
 
 if (COMPILER_RT_USE_BUILTINS_LIBRARY)
-  list(APPEND SANITIZER_COMMON_LINK_LIBS ${COMPILER_RT_BUILTINS_LIBRARY})
   string(REPLACE "-Wl,-z,defs" "" CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
 else()
   if (ANDROID)
diff --git a/compiler-rt/cmake/Modules/AddCompilerRT.cmake b/compiler-rt/cmake/Modules/AddCompilerRT.cmake
index bcb16584ff7..6a9f14ed7c5 100644
--- a/compiler-rt/cmake/Modules/AddCompilerRT.cmake
+++ b/compiler-rt/cmake/Modules/AddCompilerRT.cmake
@@ -1,5 +1,6 @@
 include(ExternalProject)
 include(CompilerRTUtils)
+include(HandleCompilerRT)
 
 function(set_target_output_directories target output_dir)
   # For RUNTIME_OUTPUT_DIRECTORY variable, Multi-configuration generators
@@ -226,6 +227,13 @@ function(add_compiler_rt_runtime name type)
       set(extra_cflags_${libname} ${TARGET_${arch}_CFLAGS} ${NO_LTO_FLAGS} ${LIB_CFLAGS})
       get_compiler_rt_output_dir(${arch} output_dir_${libname})
       get_compiler_rt_install_dir(${arch} install_dir_${libname})
+      if(COMPILER_RT_USE_BUILTINS_LIBRARY)
+        if(NOT builtins_${arch})
+            get_compiler_rt_target(${arch} target)
+            find_compiler_rt_library(builtins ${target} builtins_${arch})
+        endif()
+        set(builtins_${libname} ${builtins_${arch}})
+      endif()
     endforeach()
   endif()
 
@@ -311,6 +319,9 @@ function(add_compiler_rt_runtime name type)
         OUTPUT_NAME ${output_name_${libname}})
     set_target_properties(${libname} PROPERTIES FOLDER "Compiler-RT Runtime")
     if(LIB_LINK_LIBS)
+      if(builtins_${libname})
+        set(LIB_LINK_LIBS ${LIB_LINK_LIBS} ${builtins_${libname}})
+      endif()
       target_link_libraries(${libname} PRIVATE ${LIB_LINK_LIBS})
     endif()
     if(${type} STREQUAL "SHARED")
diff --git a/compiler-rt/cmake/Modules/HandleCompilerRT.cmake b/compiler-rt/cmake/Modules/HandleCompilerRT.cmake
index 61b7792789e..d073f65cc46 100644
--- a/compiler-rt/cmake/Modules/HandleCompilerRT.cmake
+++ b/compiler-rt/cmake/Modules/HandleCompilerRT.cmake
@@ -1,7 +1,9 @@
-function(find_compiler_rt_library name variable)
+function(find_compiler_rt_library name target variable)
   set(CLANG_COMMAND ${CMAKE_CXX_COMPILER} ${SANITIZER_COMMON_CFLAGS}
       "--rtlib=compiler-rt" "--print-libgcc-file-name")
-  if (CMAKE_CXX_COMPILER_ID MATCHES Clang AND CMAKE_CXX_COMPILER_TARGET)
+  if (target)
+    list(APPEND CLANG_COMMAND "--target=${target}")
+  elseif (CMAKE_CXX_COMPILER_ID MATCHES Clang AND CMAKE_CXX_COMPILER_TARGET)
     list(APPEND CLANG_COMMAND "--target=${CMAKE_CXX_COMPILER_TARGET}")
   endif()
   get_property(SANITIZER_CXX_FLAGS CACHE CMAKE_CXX_FLAGS PROPERTY VALUE)
diff --git a/compiler-rt/cmake/config-ix.cmake b/compiler-rt/cmake/config-ix.cmake
index f08a3c59bdb..960c8c21736 100644
--- a/compiler-rt/cmake/config-ix.cmake
+++ b/compiler-rt/cmake/config-ix.cmake
@@ -15,7 +15,7 @@ endfunction()
 check_library_exists(c fopen "" COMPILER_RT_HAS_LIBC)
 if (COMPILER_RT_USE_BUILTINS_LIBRARY)
   include(HandleCompilerRT)
-  find_compiler_rt_library(builtins COMPILER_RT_BUILTINS_LIBRARY)
+  find_compiler_rt_library(builtins "" COMPILER_RT_BUILTINS_LIBRARY)
 else()
   if (ANDROID)
     check_library_exists(gcc __gcc_personality_v0 "" COMPILER_RT_HAS_GCC_LIB)
-- 
2.24.0

