# Copyright 2014 Pierre Lejeune <superheron@gmail.com>
# Copyright 2015, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives
require github [ user="open-source-parsers" ]
require python [ blacklist=none has_bin=true has_lib=false ]
require cmake

export_exlib_phases src_compile src_test src_install

SUMMARY="An implementation of a JSON reader and writer in C++"
DESCRIPTION="
JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans
to read and write. It is easy for machines to parse and generate.
"

LICENCES="|| ( public-domain MIT )"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    run:
        !dev-libs/jsoncpp:0[<0.10.5] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !dev-libs/jsoncpp:1[<1.7.5-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-lang/python:*[>=2.6]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DJSONCPP_WITH_CMAKE_PACKAGE:BOOL=TRUE
    -DJSONCPP_WITH_POST_BUILD_UNITTEST:BOOL=FALSE
    -DJSONCPP_WITH_WARNING_AS_ERROR:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DJSONCPP_WITH_TESTS=TRUE -DJSONCPP_WITH_TESTS=FALSE' )

if ever at_least 1.8.0 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Avoid conflicts with dev-libs/json-c
        -DCMAKE_INSTALL_INCLUDEDIR=include/${PN}
        -DBUILD_STATIC_LIBS:BOOL=FALSE
    )
else
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Avoid conflicts with dev-libs/json-c
        -DINCLUDE_INSTALL_DIR=/usr/$(exhost --target)/include/${PN}
    )
fi

jsoncpp_src_compile() {
    default

    if option doc ; then
        # Needs to be run from the project's root dir
        edo pushd "${CMAKE_SOURCE}"
        ${PYTHON} doxybuild.py --doxygen=/usr/bin/doxygen
        edo popd
    fi
}

jsoncpp_src_test() {
    emake jsoncpp_readerwriter_tests
}

jsoncpp_src_install() {
    cmake_src_install

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"/dist/doxygen
        docinto html
        # Versions compiled with C++11 begin with 1 instead of 0
        dodoc -r ${PN}-api-html-1.$(ever range 2-3)/*
        edo popd
    fi

    ever at_least 1.8.0 || alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/lib/libjsoncpp.a        libjsoncpp-${SLOT}.a

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/lib/libjsoncpp.so           libjsoncpp-${SLOT}.so   \
        /usr/$(exhost --target)/lib/cmake/${PN}/${PN}Config.cmake ${PN}Config-${SLOT}.cmake \
        /usr/$(exhost --target)/lib/cmake/${PN}/${PN}Config-none.cmake ${PN}Config-none-${SLOT}.cmake \
        /usr/$(exhost --target)/lib/pkgconfig/jsoncpp.pc    jsoncpp-${SLOT}.pc      \
        /usr/$(exhost --target)/include/jsoncpp             jsoncpp-${SLOT}
}

