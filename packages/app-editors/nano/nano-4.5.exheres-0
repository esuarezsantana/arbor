# Copyright 2008 Stephen P. Becker <spbecker@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="GNU nano text editor"
HOMEPAGE="https://www.${PN}-editor.org"
DOWNLOADS="${HOMEPAGE}/dist/v$(ever major)/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    ( linguas: bg ca cs da de eo es eu fi fr ga gl hr hu id it ja ko ms nb nl nn pl pt pt_BR ro ru
               sl sr sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        sys-apps/file
        sys-libs/ncurses
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-browser
    --enable-color
    --enable-comment
    --enable-extra
    --enable-help
    --enable-histories
    --enable-justify
    --enable-libmagic
    --enable-linenumbers
    --enable-mouse
    --enable-multibuffer
    --enable-nanorc
    --enable-nls
    --enable-operatingdir
    --enable-speller
    --enable-tabcomp
    --enable-utf8
    --enable-wordcomp
    --disable-tiny
)

src_install() {
    default

    insinto /etc
    newins doc/sample.nanorc nanorc

    # enable syntax coloring
    edo sed \
        -e '/^# include /s:# *::' \
        -i "${IMAGE}"/etc/nanorc
}

