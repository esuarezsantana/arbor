# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Small utility displaying info about file formats"
HOMEPAGE="http://www.darwinsys.com/${PN}"

# We mirror the archive on our server because ftp is often blocked
# by firewalls (e.g. for our arm build server)
# DOWNLOADS="ftp://ftp.astron.com/pub/${PN}/${PNV}.tar.gz"
DOWNLOADS="https://dev.exherbo.org/distfiles/${PNV}.tar.gz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="parts: binaries data development documentation libraries"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Limit-the-number-of-elements-in-a-vector-found-by-os.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-zlib
    # doesn't work with sydbox
    --disable-libseccomp
    --disable-static
)
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( MAINT PORTING )

src_install() {
    default

    edo rmdir "${IMAGE}"/usr/share/man/man5

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share/misc
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

